require 'test_helper'

class SchooleManagersControllerTest < ActionController::TestCase
  setup do
    @schoole_manager = schoole_managers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:schoole_managers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create schoole_manager" do
    assert_difference('SchooleManager.count') do
      post :create, schoole_manager: { comment: @schoole_manager.comment, schoole_id: @schoole_manager.schoole_id, score: @schoole_manager.score, user_id: @schoole_manager.user_id }
    end

    assert_redirected_to schoole_manager_path(assigns(:schoole_manager))
  end

  test "should show schoole_manager" do
    get :show, id: @schoole_manager
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @schoole_manager
    assert_response :success
  end

  test "should update schoole_manager" do
    patch :update, id: @schoole_manager, schoole_manager: { comment: @schoole_manager.comment, schoole_id: @schoole_manager.schoole_id, score: @schoole_manager.score, user_id: @schoole_manager.user_id }
    assert_redirected_to schoole_manager_path(assigns(:schoole_manager))
  end

  test "should destroy schoole_manager" do
    assert_difference('SchooleManager.count', -1) do
      delete :destroy, id: @schoole_manager
    end

    assert_redirected_to schoole_managers_path
  end
end
