json.array!(@college_managers) do |college_manager|
  json.extract! college_manager, :id, :user_id, :collegeaand_id, :comment, :score
  json.url college_manager_url(college_manager, format: :json)
end
