json.array!(@assessments) do |assessment|
  json.extract! assessment, :id, :one, :two, :three, :four, :text, :five, :six, :seven, :eight, :nine, :ten, :user_id
  json.url assessment_url(assessment, format: :json)
end
