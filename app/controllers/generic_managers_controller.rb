class GenericManagersController < ApplicationController
  before_action :set_generic_manager, only: [:show, :edit, :update, :destroy]

  # GET /generic_managers
  # GET /generic_managers.json
  def index
    @generic_managers = GenericManager.all
    @generics = Generic.all
  end

  # GET /generic_managers/1
  # GET /generic_managers/1.json
  def show
  end

  # GET /generic_managers/new
  def new
    @generic_manager = GenericManager.new
    @generic_managers = GenericManager.all
    @generics = Generic.all
    @users = User.all
  end

  # GET /generic_managers/1/edit
  def edit
    @generics = Generic.all
    @generic_managers = GenericManager.all
    @users = User.all
  end

  # POST /generic_managers
  # POST /generic_managers.json
  def create
    @generic_manager = GenericManager.new(generic_manager_params)

    respond_to do |format|
      if @generic_manager.save
        format.html { redirect_to generics_path, notice: 'Score was successfully added.' }
        format.json { render :show, status: :created, location: @generic_manager }
      else
        format.html { render :new }
        format.json { render json: @generic_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /generic_managers/1
  # PATCH/PUT /generic_managers/1.json
  def update
    respond_to do |format|
      if @generic_manager.update(generic_manager_params)
        format.html { redirect_to generics_path, notice: 'Score was successfully updated.' }
        format.json { render :show, status: :ok, location: @generic_manager }
      else
        format.html { render :edit }
        format.json { render json: @generic_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /generic_managers/1
  # DELETE /generic_managers/1.json
  def destroy
    @generic_manager.destroy
    respond_to do |format|
      format.html { redirect_to generic_managers_url, notice: 'Score was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_generic_manager
      @generic_manager = GenericManager.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def generic_manager_params
      params.require(:generic_manager).permit(:user_id, :generic_id, :comment, :score)
    end
end
