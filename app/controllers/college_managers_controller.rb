class CollegeManagersController < ApplicationController
  before_action :set_college_manager, only: [:show, :edit, :update, :destroy]

  # GET /college_managers
  # GET /college_managers.json
  def index
    @college_managers = CollegeManager.all
    @collegeaands = Collegeaand.all
  end

  # GET /college_managers/1
  # GET /college_managers/1.json
  def show
  end

  # GET /college_managers/new
  def new
    @college_manager = CollegeManager.new
    @college_managers = CollegeManager.all
    @collegeaands = Collegeaand.all
    @users = User.all
  end

  # GET /college_managers/1/edit
  def edit
    @collegeaands = Collegeaand.all
    @college_managers = CollegeManager.all
    @users = User.all
  end

  # POST /college_managers
  # POST /college_managers.json
  def create
    @college_manager = CollegeManager.new(college_manager_params)

    respond_to do |format|
      if @college_manager.save
        format.html { redirect_to collegeaands_path, notice: 'Score was successfully created.' }
        format.json { render :show, status: :created, location: @college_manager }
      else
        format.html { render :new }
        format.json { render json: @college_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /college_managers/1
  # PATCH/PUT /college_managers/1.json
  def update
    respond_to do |format|
      if @college_manager.update(college_manager_params)
        format.html { redirect_to collegeaands_path, notice: 'Score was successfully updated.' }
        format.json { render :sho1w, status: :ok, location: @college_manager }
      else
        format.html { render :edit }
        format.json { render json: @college_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /college_managers/1
  # DELETE /college_managers/1.json
  def destroy
    @college_manager.destroy
    respond_to do |format|
      format.html { redirect_to college_managers_url, notice: 'Score was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_college_manager
      @college_manager = CollegeManager.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def college_manager_params
      params.require(:college_manager).permit(:user_id, :collegeaand_id, :comment, :score)
    end
end
