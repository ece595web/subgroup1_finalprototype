class CollegeaandsController < ApplicationController
  before_action :set_collegeaand, only: [:show, :edit, :update, :destroy]

  # GET /collegeaands
  # GET /collegeaands.json
  def index
    @collegeaands = Collegeaand.all
    @users = User.all
    @college_managers = CollegeManager.all
  end

  # GET /collegeaands/1
  # GET /collegeaands/1.json
  def show
  end

  # GET /collegeaands/new
  def new
    @collegeaand = Collegeaand.new
  end

  # GET /collegeaands/1/edit
  def edit
  end

  # POST /collegeaands
  # POST /collegeaands.json
  def create
    @collegeaand = Collegeaand.new(collegeaand_params)

    respond_to do |format|
      if @collegeaand.save
        format.html { redirect_to collegeaands_path, notice: 'College of Arts and Sciences Form was successfully created.' }
        format.json { render :show, status: :created, location: @collegeaand }
      else
        format.html { render :new }
        format.json { render json: @collegeaand.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /collegeaands/1
  # PATCH/PUT /collegeaands/1.json
  def update
    respond_to do |format|
      if @collegeaand.update(collegeaand_params)
        format.html { redirect_to collegeaands_path, notice: 'College of Arts and Sciences Form was successfully updated.' }
        format.json { render :show, status: :ok, location: @collegeaand }
      else
        format.html { render :edit }
        format.json { render json: @collegeaand.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /collegeaands/1
  # DELETE /collegeaands/1.json
  def destroy
    @collegeaand.destroy
    respond_to do |format|
      format.html { redirect_to collegeaands_url, notice: 'College of Arts and Sciences Form was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_collegeaand
      @collegeaand = Collegeaand.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def collegeaand_params
      params.require(:collegeaand).permit(:one, :two, :three, :four, :five, :six, :seven, :eight, :nine, :ten, :eleven, :twelve, :user_id)
    end
end
