class CreateGenericManagers < ActiveRecord::Migration
  def change
    create_table :generic_managers do |t|
      t.references :user, index: true, foreign_key: true
      t.references :generic, index: true, foreign_key: true
      t.text :comment
      t.decimal :score

      t.timestamps null: false
    end
  end
end
