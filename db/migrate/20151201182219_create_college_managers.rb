class CreateCollegeManagers < ActiveRecord::Migration
  def change
    create_table :college_managers do |t|
      t.references :user, index: true, foreign_key: true
      t.references :collegeaand, index: true, foreign_key: true
      t.text :comment
      t.decimal :score

      t.timestamps null: false
    end
  end
end
