class CreateAssessments < ActiveRecord::Migration
  def change
    create_table :assessments do |t|
      t.date :one
      t.string :two
      t.string :three
      t.string :four
      t.string :text
      t.date :five
      t.text :six
      t.text :seven
      t.text :eight
      t.text :nine
      t.text :ten
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
