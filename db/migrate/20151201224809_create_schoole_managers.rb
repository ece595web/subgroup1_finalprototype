class CreateSchooleManagers < ActiveRecord::Migration
  def change
    create_table :schoole_managers do |t|
      t.references :user, index: true, foreign_key: true
      t.references :schoole, index: true, foreign_key: true
      t.text :comment
      t.decimal :score

      t.timestamps null: false
    end
  end
end
